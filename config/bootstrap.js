/**
 * Bootstrap
 * (sails.config.bootstrap)
 *
 * An asynchronous bootstrap function that runs before your Sails app gets lifted.
 * This gives you an opportunity to set up your data model, run jobs, or perform some special logic.
 *
 * For more information on bootstrapping your app, check out:
 * http://sailsjs.org/#!/documentation/reference/sails.config/sails.config.bootstrap.html
 */

var createAdmin = function(){
	var admin, permAdmin, permUser;
	return Users.create({username: 'admin', password:'viewlead888'})
	.then(function(user){
		console.log('admin password=',user.password);
		delete user.password;
		admin = user;
		return Permissions.create({name:'ADMIN'});
	})
	.then(function(permission){
		permAdmin = permission;
		return Permissions.create({name:'USER'});
	})
	.then(function(permission){
		permUser = permission;
		admin.permissions.add(permAdmin.id);
		admin.permissions.add(permUser.id);
		return admin.save();
	})
	.catch(function(error){
		console.error(error);
	});
}

module.exports.bootstrap = function(cb) {

	// Create the 1st user account
	Users.count()
	.then(function(number){
		console.log('Number of Users=',number);
		if(number==0){
			return createAdmin();
		}else{
			return;
		}
	})
	.finally(function(){
	  // It's very important to trigger this callback method when you are finished
	  // with the bootstrap!  (otherwise your server will never lift, since it's waiting on the bootstrap)
	  cb();
	});
};
