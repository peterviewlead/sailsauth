/**
 * ExtUser.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  connection:'extMysqlServer',
  // connection:'extSqlserver',
  migrate: 'safe',
  autoPK: false,
  tableName:'authorization_people',
  autoCreatedAt: false,
  autoUpdatedAt: false,

  attributes: {
  	username: {
  		type: 'string',
  		unique: true,
  		required: true,
  		columnName: 'Name'
  	},
  	password: {
  		type: 'string',
  		required: true,
  		columnName: 'Password'

  	}
  }
};

