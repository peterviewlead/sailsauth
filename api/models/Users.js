/**
 * Users.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

// We don't want to store password with out encryption
var bcrypt = require('bcrypt');
var Q = require('q');

module.exports = {

  schema: true,

  attributes: {
    username: {
      type: 'string',
      required: 'true',
      unique: true // Yes unique one
    },

    password: {
      type: 'string'
    },

    permissions: {
      collection: 'permissions',
      via: 'users'
    },

    // We don't wan't to send back encrypted password either
    toJSON: function () {
      var obj = this.toObject();
      delete obj.password;
      return obj;
    }
  },
  // Here we encrypt password before creating a User
  beforeCreate : function (values, next) {
    Users.encryptPassword(values.password)
    .then(function(hash){
      values.password = hash;
      next();
    })
    .catch(function(error){
      next(err);
    });
  },

  changePassword: function(user, password){
    Users.encryptPassword(password)
    .then(function(hash){
      user.password = hash;
      return user.save();
    });
  },

  encryptPassword : function(password){
    var deferred = Q.defer();
    bcrypt.genSalt(10, function (err, salt) {
        if(err) deferred.reject(err);
        bcrypt.hash(password, salt, function (err, hash) {
          if(err) deferred.reject(err);
          deferred.resolve(hash);
        })
    })
    return deferred.promise;
  },

  comparePassword : function (password, user) {
    var deferred = Q.defer();
    bcrypt.compare(password, user.password, function (err, match) {
      console.log('comparePassword err!',err,match);
      if(err) deferred.reject(err);
      if(match) {
        console.log('comparePassword MATCH!!');
        deferred.resolve(true);
      } else {
        deferred.resolve(false);
      }
    })
    return deferred.promise;
  }
};