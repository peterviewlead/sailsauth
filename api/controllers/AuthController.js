/**
 * AuthController
 *
 * @description :: Server-side logic for managing auths
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
var Q = require('q');

var authProcess = function(username, password){
  var theUser;

  return Q.when()
  .then(function(){
    if (!username || !password) {
      throw {code: 401, err: 'username and password required'};
    }
  })
  .then(function(){
    return Users.findOne({username: username}).populate('permissions')
  })
  .then(function(user){
    console.log('user:',user);
    if (!user) {
        console.log('user not found');
        throw {code:401, err:'invalid username or password'};
    }else{
      theUser = user;
      return Users.comparePassword(password, user);
    }
  })
  .then(function (valid) {
    console.log('valid?',valid);
    if (!valid) {
      return ExtUser.findOne({username:username, password:password})
      .then(function(extUser){
        console.log('extUser=',extUser);
        if(!extUser){
          throw {code:401, err:'invalid username or password'};
        }else{
          return theUser;
        }
      });
    } else {
      return theUser;
    }
  })
  .then(function(user){
    console.log('PASS!',user);
    var permissions = [];
    for(var i=0; i<user.permissions.length; i++){
      console.log('user.permissions',i, user.permissions[i].name);
      permissions.push(user.permissions[i].name);
    }
    var json ={
      user: { username: user.username },
      authLevel: permissions,
      token: jwToken.issue({username : user.username, authLevel: permissions })
    };
    return json;
  });

}


module.exports = {
  index: function (req, res) {
    var username = req.param('username');
    var password = req.param('password');

    authProcess(username, password)
    .then(function(json){
      res.cookie('authorization', json.token, {httpOnly: true});
      return res.json(json);
    })
    .catch(function(error){
      console.log('ERROR!',error);
      if(error && error.code){
        return res.json(error.code, error);
      }else{
        return res.status(500).send('500');
      }
    });
  },
  login: function(req,res){
    res.view();
  },
  doLogin: function(req, res){
    var username = req.param('username');
    var password = req.param('password');

    authProcess(username, password)
    .then(function(json){
      res.cookie('authorization', json.token, {httpOnly: true});
      return res.redirect('/'); //TODO: now always redirect to the front page.
    })
    .catch(function(error){
      console.log('ERROR!',error);
      if(error && error.code){
        return res.forbidden();
      }else{
        return res.serverError();
      }
    });
  },
  logout: function(req, res){
    res.clearCookie('authorization').end();
  },
  logoutPage: function(req, res){
    res.clearCookie('authorization').end();
    res.redirect('/auth/login');
  }

};