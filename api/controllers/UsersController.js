/**
 * UsersController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
  create: function (req, res) {
    Users.create(req.body).exec(function (err, user) {
      if (err) {
        return res.json(err.status, {err: err});
      }
      // If user created successfuly we return user and token as response
      if (user) {
        // NOTE: payload is { id: user.id}
        res.json(200, {user: user, token: jwToken.issue({id: user.id})});
      }
    });
  },

/* Special action for changing password */
  changePassword: function(req, res){
    var userId = req.param('userId');
    var password = req.param('password');

    if(!userId || !password){
      console.log('invalide parameter');
      return res.badRequest();
    }
    Users.findOne({id:userId})
    .then(function(user){
      if(!user){
        throw new Error('User not found');
      }else{
        return Users.changePassword(user, password);
      }
    })
    .then(function(){
      res.send('ok');
    })
    .catch(function(error){
      console.error('Caught error');
      res.badRequest(error);
    });


  }
};