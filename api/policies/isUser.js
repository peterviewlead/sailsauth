module.exports = function (req, res, next) {
  var token = req.token;

  if(token.authLevel.indexOf('USER')==-1){
  	console.error('NOT USER!');
  	return res.redirect('/auth/login');
  }
  next();

};