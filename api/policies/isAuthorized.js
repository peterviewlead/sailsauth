/**
 * isAuthorized
 *
 * @description :: Policy to check if user is authorized with JSON web token
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Policies
 */

module.exports = function (req, res, next) {
  var token;

  if (req.headers && req.headers.authorization) {
    token = req.headers.authorization
  } else if (req.param('token')) {
    token = req.param('token');
    // We delete the token from param to not mess with blueprints
    delete req.query.token;
  } else if(req.cookies.authorization){
    token = req.cookies.authorization;
  } else {
    return res.json(401, {err: 'No Authorization header was found'});
  }

  jwToken.verify(token, function (err, token) {
    // if (err) return res.json(401, {err: 'Invalid Token!'});
    if (err) return res.json(401, {err: 'No Authorization header was found'});
    req.token = token; // This is the decrypted token or the payload you provided
    console.log('isAuthorized:',token);
    next();
  });
};