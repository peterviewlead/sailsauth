module.exports = function (req, res, next) {
  var token = req.token;

  if(token.authLevel.indexOf('ADMIN')==-1){
  	console.error('NOT ADMIN!');
  	return res.redirect('/auth/login');
  }
  next();

};