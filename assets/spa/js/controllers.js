angular.module('controllers',[])
.controller('MainCtrl', function($scope,AuthService){
	console.log('MAIN!!!');
	$scope.logout = function(){
		AuthService.logout();
	}

})
.controller('LoginCtrl', function($scope, AuthService, $http){
	console.log('Login!!!');
	$scope.credentials = { username:'', password:'' }
	$scope.login = function(credentials){
		AuthService.login(credentials);
	}
})
.controller('AdminCtrl', function($scope){
	console.log('ADMIN!!!');
})
;