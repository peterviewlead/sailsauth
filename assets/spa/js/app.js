var app = angular.module('QcServerApp',['controllers','services','ui.router','mgcrea.ngStrap','hitmands.auth','LocalStorageModule']);

app.config(function($stateProvider, $urlRouterProvider,AuthServiceProvider, $httpProvider) {

// Set API ENDPOINTS
  AuthServiceProvider.useRoutes({
          login: '/auth',
          logout: '/auth/logout',
          fetch: '/api/v1/users/logged-in'
      });

  // Callback that handles the $http Response and returns the AuthData to the AuthService
  AuthServiceProvider.parseHttpAuthData(function(data, headers, statusCode) {
      return {
          user: data.user,
          authLevel: data.authLevel,
          token: data.token
      };
  });
  AuthServiceProvider.tokenizeHttp('authorization');

  $stateProvider
    .state('main', {
      url: '/main',
      templateUrl: 'templates/main.html',
      controller: 'MainCtrl',
      data:{
        authLevel:['ADMIN']
      }

    })
    .state('login', {
      url: '/login',
      templateUrl: 'templates/login.html',
      controller: 'LoginCtrl',
      params:{
        toState: null,
        toParams: null
      }
    })
    .state('admin', {
      url: '/admin',
      templateUrl: 'templates/admin.html',
      controller: 'AdminCtrl',
      data:{
        authLevel:['ADMIN']
      }
    })

    ;

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/main');


  $httpProvider.interceptors.push(function($q, $rootScope) {
    return {
      'responseError': function(rejection) {
        if(rejection.status==401){
          $rootScope.$emit('auth:NotAuthenticated',response);
        }else if(rejection.status==403){
          $rootScope.$emit('auth:NotAuthorized',response);
        }
        return $q.reject(rejection);
      }
    };
  });

})
.run(function($rootScope, $state, localStorageService, AuthService){


  $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error){
    console.log('$stateChangeError', toState, fromState, error);
    $state.go('login', { toState:toState, toParams:toParams });
  });

  $rootScope.$on('hitmands.auth:login.resolved', function(event, result){
    console.log('login resolved', result);
    localStorageService.set('authData', { user: result.data.user, authLevel: result.data.authLevel, token: result.data.token});
    $state.go('main');
  });

  $rootScope.$on('hitmands.auth:logout.resolved', function(event, result){
    console.log('logout resolved', result);
    localStorageService.remove('authData');
    $state.go('login');
  });


  $rootScope.$on('auth:NotAuthenticated', function(){
    $state.go('login');
  });

  $rootScope.$on('auth:NotAuthorized', function(){
    $state.go('login');
  });

// Load token when initialize.
  var authData = localStorageService.get('authData');
  if(authData){
    AuthService.setCurrentUser(authData.user, authData.authLevel, authData.token);
  }

})
